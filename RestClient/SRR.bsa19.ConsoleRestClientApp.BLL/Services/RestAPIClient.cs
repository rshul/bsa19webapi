﻿using Newtonsoft.Json;
using SRR.bsa19.ConsoleRestClientApp.Shared.Models;
using SRR.bsa19.ConsoleRestClientApp.Shared.Types;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace SRR.bsa19.ConsoleRestClientApp.BLL.Services
{
    public class RestAPIClient
    {

        static private readonly HttpClient _client;

        static RestAPIClient()
        {
            var handler = new HttpClientHandler() // only for debugging
            {
                ServerCertificateCustomValidationCallback = delegate { return true; },
            };

            HttpClient client = new HttpClient(handler);
            client.BaseAddress = new Uri("https://localhost:5001/api/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            
            _client = client;

        }


        public static async Task<bool> CreateTransportAsync(TransportTypes transportType)
        {
            HttpResponseMessage response = await _client.PostAsJsonAsync(
                "transports", transportType);
            
            return response.IsSuccessStatusCode;
        }

        public static async Task<IEnumerable<T>> GetObjectsAsync<T>(string path)
        {
            IEnumerable<T> objects = null;
            HttpResponseMessage response = await _client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                objects = await response.Content.ReadAsAsync<IEnumerable<T>>();
            }
            return objects;
        }

        public static async Task<T> GetNumberAsync<T>(string path)
        {
            T number = default(T);
            HttpResponseMessage response = await _client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                number = await response.Content.ReadAsAsync<T>();
            }
            return number;

        }

        public static async Task<bool> UpdateTransportAsync(Guid transportId, double topUpSum)
        {
            var content = new ObjectContent(typeof(double), topUpSum, new JsonMediaTypeFormatter(), "application/json");
            HttpResponseMessage response = await _client.PatchAsync(
                $"transports/{transportId}", content);
            return response.IsSuccessStatusCode;
        }

        public static async Task<bool> DeleteTransportAsync(Guid transportId)
        {
            HttpResponseMessage response = await _client.DeleteAsync(
                $"transports/{transportId}");
            return response.IsSuccessStatusCode;
        }
    }
}
