﻿using SRR.bsa19.ConsoleRestClientApp.Shared.Interfaces;
using SRR.bsa19.ConsoleRestClientApp.Shared.Models;
using SRR.bsa19.ConsoleRestClientApp.Shared.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace SRR.bsa19.ConsoleRestClientApp.BLL.Services
{
    public class UserInteraction : IUserInteraction
    {
        public double BalanceOfParking
        {
            get
            {
                return RestAPIClient.GetNumberAsync<double>("parking/balance").Result;
            }
        }

        public double LastMinuteIncome { get { return RestAPIClient.GetNumberAsync<double>("parking/lastIncome").Result; } } 

        public int FreeLotsNumber
        {
            get
            {
                return RestAPIClient.GetNumberAsync<int>("parking/emptyLots").Result;
            }
        }

        public int BusyLotsNumber
        {
            get
            {
                return RestAPIClient.GetNumberAsync<int>("parking/busyLots").Result;
            }
        }

        public IEnumerable<ITransportItem> ParkedTransport
        {
            get
            {
                return RestAPIClient.GetObjectsAsync<TransportItem>("transports").Result;
            }
        }

        

        public void OutputLastMinuteTransactions()
        {
           IEnumerable<Transaction> transactions = RestAPIClient.GetObjectsAsync<Transaction>("transactions/lastminute").Result;
            foreach (var transaction in transactions)
            {
                Console.WriteLine(transaction);
            }
        }

        public void OutputTransactionFromFile()
        {
            IEnumerable<string> transactions = RestAPIClient.GetObjectsAsync<string>("transactions/fromfile").Result;
            foreach (var transaction in transactions)
            {
                Console.WriteLine(transaction);
            }
        }

        public bool PutTransportInParking(TransportTypes transportType)
        {
            bool isSuccess = RestAPIClient.CreateTransportAsync(transportType).Result;
            return isSuccess;
        }

        public bool RemoveTransportFromParking(Guid transportId)
        {
            bool isSuccess = RestAPIClient.DeleteTransportAsync(transportId).Result;
            return isSuccess;
        }

        public bool TopUpTransportAccount(Guid transportID, double topUpSum)
        {
            bool isSuccess = RestAPIClient.UpdateTransportAsync(transportID, topUpSum).Result;
            return isSuccess;
        }
    }
}
