﻿using SRR.bsa19.ConsoleRestClientApp.Shared.Interfaces;
using SRR.bsa19.ConsoleRestClientApp.Shared.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleUIApp
{
    public static class MenuCreator
    {
        public static IUserInteraction CommandsProvider { get; set; }

        private static readonly List<string> menuItems = new List<string>
            {
                "Show parking balance",
                "Show income for the last minute",
                "Show number of free spaces",
                "Print transactions for the last minute",
                "Print transactions from file",
                "Put transport in parking lot",
                "Remove transport from parking",
                "Top up transport account",
                "Show transport inforamtion",
                "exit"
            };

        static public void MainMenu()
        {
            int choice;
            do
            {
                Console.SetCursorPosition(0, 0);
                PrintMenu(menuItems);
                choice = GetMenuChoice(0, menuItems.Count);
                Console.Clear();
                Console.SetCursorPosition(0, menuItems.Count);
                Console.CursorVisible = true;
                RunCommand(choice);
            } while (choice != menuItems.Count - 1);

        }

        private static void RunCommand(int choice)
        {
            string message = String.Empty;
            switch (choice)
            {
                case 0:
                    Console.WriteLine($"Parking balance is {CommandsProvider.BalanceOfParking}");
                    break;
                case 1:
                    Console.WriteLine($"The income for the last minute is {CommandsProvider.LastMinuteIncome}");
                    break;
                case 2:
                    Console.WriteLine($"The  number of free spaces is {CommandsProvider.FreeLotsNumber}");
                    break;
                case 3:
                    CommandsProvider.OutputLastMinuteTransactions();
                    break;
                case 4:
                    CommandsProvider.OutputTransactionFromFile();
                    break;
                case 5:
                    message = CommandsProvider.PutTransportInParking(GetTypeOfTransport()) ?
                        "Transport successfully added" : "ERROR! You can't add transport";
                    Console.WriteLine(message);
                    break;
                case 6:
                    int rangeOfChoice = CommandsProvider.BusyLotsNumber;
                    if (rangeOfChoice == 0)
                    {
                        Console.WriteLine("Parking is empty");
                        break;
                    }
                    Guid transportIDToRemove = GetTransportIdNumber();
                    bool result = CommandsProvider.RemoveTransportFromParking(transportIDToRemove);
                    Console.WriteLine($"Result: {result}");
                    break;
                case 7:
                    PrintTransportsInfo();
                    (Guid transportId, double topUpSum) = GetTopUpData();
                    message = CommandsProvider.TopUpTransportAccount(transportId, topUpSum) ?
                        "Transport account topped up" : "ERROR! Wrong operation. Try again.";
                    Console.WriteLine(message);
                    break;
                case 8:
                    PrintTransportsInfo();
                    break;
                default:
                    Console.WriteLine("Not valid commmand");
                    break;
            }
        }

        private static (Guid, double) GetTopUpData()
        {
            Console.WriteLine($"Please, enter Transport ID: ");
            Guid guid = GetGuidValueFromInput();
            Console.WriteLine($"Please, enter top up sum:");
            double topUpSum = GetDoubleValueFromInput();
            return (guid, topUpSum);
        }

        private static void PrintTransportsInfo()
        {
            foreach (var transport in CommandsProvider.ParkedTransport)
            {
                Console.WriteLine(transport);
            }
        }

        private static Guid GetTransportIdNumber()
        {
            Console.WriteLine($"Please enter Transport ID number");
            Guid guid = GetGuidValueFromInput();
            return guid;

        }

        private static TransportTypes GetTypeOfTransport()
        {
            var values = Enum.GetValues(typeof(TransportTypes));
            Console.WriteLine("Select type of transport");
            int cursorVerticalPosition = Console.CursorTop;
            foreach (var item in values)
            {
                Console.WriteLine($"   [{(int)item}] {item}");
            }
            int choice;
            do
            {
                choice = GetMenuChoice(cursorVerticalPosition, values.Length);
            } while (!Enum.IsDefined(typeof(TransportTypes), choice));

            Console.Clear();
            Console.SetCursorPosition(0, cursorVerticalPosition + values.Length);
            return (TransportTypes)choice;

        }

        private static int GetMenuChoiceSimple(int maxValue)
        {
            Console.WriteLine();
            int resultInt;
            bool isValidInput;
            do
            {
                resultInt = GetIntValueFromInput();
                isValidInput = !(resultInt < 0 || resultInt > maxValue);
                string message = isValidInput ? "" : $"-1 < value <= {maxValue}";
                Console.WriteLine(message);
            } while (!isValidInput);
            return resultInt;
        }

        private static int GetIntValueFromInput()
        {
            int resultInt;
            bool isParsedOK;
            do
            {
                isParsedOK = int.TryParse(Console.ReadLine(), out resultInt);
            } while (!isParsedOK);
            return resultInt;
        }
        private static double GetDoubleValueFromInput()
        {
            double resultDouble;
            bool isParsedOK;
            do
            {
                isParsedOK = double.TryParse(Console.ReadLine(), out resultDouble);
            } while (!isParsedOK);
            return resultDouble;
        }
        private static Guid GetGuidValueFromInput()
        {
            Guid resultGuid;
            bool isParsedOK;
            do
            {
                isParsedOK = Guid.TryParse(Console.ReadLine(), out resultGuid);
            } while (!isParsedOK);
            return resultGuid;
        }

        private static int GetMenuChoice(int initialVerticalCursorPosition, int menuRange)
        {
            ConsoleKeyInfo pressedKey;
            int pointerInitialPosition = initialVerticalCursorPosition;
            menuRange--;
            int pointerPosition = pointerInitialPosition;
            Console.CursorVisible = false;
            do
            {
                ArrowAt(0, pointerPosition);
                pressedKey = Console.ReadKey(true);
                if (pressedKey.Key == ConsoleKey.DownArrow)
                {
                    DeleteArrowAt(0, pointerPosition);
                    pointerPosition = pointerPosition == pointerInitialPosition + menuRange ? pointerInitialPosition : pointerPosition + 1;
                }
                else if (pressedKey.Key == ConsoleKey.UpArrow)
                {
                    DeleteArrowAt(0, pointerPosition);
                    pointerPosition = pointerPosition == pointerInitialPosition ? pointerInitialPosition + menuRange : pointerPosition - 1;
                }
            } while (pressedKey.Key != ConsoleKey.Enter);

            DeleteArrowAt(0, pointerPosition);
            return pointerPosition - pointerInitialPosition;
        }

        private static void PrintMenu(List<string> lsString)
        {
            for (int i = 0; i < lsString.Count; i++)
            {
                Console.WriteLine($"  [{i}]\t{lsString[i]}");
            }
        }

        private static void ArrowAt(int x, int y)
        {
            WriteAt("=", x, y);
            WriteAt(">", x + 1, y);
        }

        private static void DeleteArrowAt(int x, int y)
        {
            ClearAt(x, y);
            ClearAt(x + 1, y);
        }

        private static void WriteAt(string s, int x, int y)
        {
            int origRow = 0;
            int origCol = 0;

            try
            {
                Console.SetCursorPosition(origCol + x, origRow + y);
                Console.Write(s);
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.Clear();
                Console.WriteLine(e.Message);
            }
        }

        private static void ClearAt(int x, int y)
        {
            int origRow = 0;
            int origCol = 0;

            try
            {
                Console.SetCursorPosition(origCol + x, origRow + y);
                Console.Write(" ");
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.Clear();
                Console.WriteLine(e.Message);
            }
        }
    }
}
