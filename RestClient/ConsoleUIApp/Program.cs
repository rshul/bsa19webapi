﻿using SRR.bsa19.ConsoleRestClientApp.BLL.Services;
using SRR.bsa19.ConsoleRestClientApp.Shared.Interfaces;
using SRR.bsa19.ConsoleRestClientApp.Shared.Models;
using SRR.bsa19.ConsoleRestClientApp.Shared.Types;
using System;
using System.Collections.Generic;

namespace ConsoleUIApp
{
    class Program
    {
        static void Main(string[] args)
        {
            /**
             * For navigation menu use arrow up and down;
             * for selection menu press Enter key
             * 
             * */
            MenuCreator.CommandsProvider = new UserInteraction();
            MenuCreator.MainMenu();

                Console.ReadLine();
        }
    }
}
