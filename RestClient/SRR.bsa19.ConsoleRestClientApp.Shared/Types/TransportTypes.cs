﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRR.bsa19.ConsoleRestClientApp.Shared.Types
{
    public enum TransportTypes
    {
        PessengerCar,
        Truck,
        Bus,
        Bike
    }
}
