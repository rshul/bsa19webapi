﻿using SRR.bsa19.ConsoleRestClientApp.Shared.Models;
using SRR.bsa19.ConsoleRestClientApp.Shared.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace SRR.bsa19.ConsoleRestClientApp.Shared.Interfaces
{
    public interface ITransportItem
    {
        Guid TransportItemID { get; }
        TransportTypes TransportType { get; }
        double Balance { get; }
    }
}
