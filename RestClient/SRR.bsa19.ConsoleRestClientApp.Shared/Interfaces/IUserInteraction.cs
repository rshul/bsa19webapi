﻿using SRR.bsa19.ConsoleRestClientApp.Shared.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace SRR.bsa19.ConsoleRestClientApp.Shared.Interfaces
{
    public interface IUserInteraction
    {
        double BalanceOfParking { get; }
        double LastMinuteIncome { get; }
        int FreeLotsNumber { get; }
        int BusyLotsNumber { get; }
        IEnumerable<ITransportItem> ParkedTransport { get; }
        void OutputLastMinuteTransactions();
        void OutputTransactionFromFile();
        bool PutTransportInParking(TransportTypes transportType);
        bool RemoveTransportFromParking(Guid transportId);
        bool TopUpTransportAccount(Guid transportID, double topUpSum);

    }
}
