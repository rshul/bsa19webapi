﻿using SRR.bsa19.ConsoleRestClientApp.Shared.Interfaces;
using SRR.bsa19.ConsoleRestClientApp.Shared.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace SRR.bsa19.ConsoleRestClientApp.Shared.Models
{
    public class TransportItem : ITransportItem
    {
        public Guid TransportItemID { get; set; }

        public double Balance { get; set; }

        public TransportTypes TransportType { get; set; }

        public override string ToString()
        {
            return $"# {TransportItemID.ToString("B")}; balance: {Balance}; type: {TransportType};";
        }

    }
}
