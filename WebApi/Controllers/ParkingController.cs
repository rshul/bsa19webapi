using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.Interfaces;
using WebApi.TypesEnum;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private readonly IUserInteraction parkingService;
        public ParkingController(IUserInteraction parkingSevice)
        {
            this.parkingService = parkingSevice;
        }
        // GET api/values
        [HttpGet("balance")]
        public ActionResult<double> Balance()
        {
            return Ok(parkingService.BalanceOfParking);
        }

        [HttpGet("lastIncome")]
        public ActionResult<double> LastIncome()
        {
            return Ok(parkingService.LastMinuteIncome);
        }

        [HttpGet("emptyLots")]
        public ActionResult<int> EmptyLots()
        {
            return Ok(parkingService.FreeLotsNumber);
        }

        [HttpGet("busyLots")]
        public ActionResult<int> BusyLots()
        {
            return Ok(parkingService.BusyLotsNumber);
        }

    }
}