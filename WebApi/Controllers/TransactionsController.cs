using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.BL.Models;
using WebApi.Interfaces;


namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly IUserInteraction parkingService;
        public TransactionsController(IUserInteraction parkingSevice)
        {
            this.parkingService = parkingSevice;
        }
        // GET api/values
        [HttpGet("lastminute")]
        public ActionResult<IEnumerable<Transaction>> LastMinute()
        {
            return Ok(parkingService.OutputLastMinuteTransactions());
        }

        [HttpGet("fromfile")]
        public ActionResult<IEnumerable<string>> FromFile()
        {
            return Ok(parkingService.OutputTransactionFromFile());
        }
    }
}