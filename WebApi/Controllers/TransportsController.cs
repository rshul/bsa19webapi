using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.Interfaces;
using WebApi.TypesEnum;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransportsController : ControllerBase
    {
        private readonly IUserInteraction parkingService;
        public TransportsController(IUserInteraction parkingSevice)
        {
            this.parkingService = parkingSevice;
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<ITransportItem>> Get()
        {
            return Ok(parkingService.ParkedTransport);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(Guid id)
        {
            var targetTransport = parkingService.ParkedTransport.FirstOrDefault(i => i.TransportItemID == id);
            if (targetTransport == null)
            {
                return NotFound();
            }
            return Ok(targetTransport);
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody] TransportTypes value)
        {
            bool isValidEnum = Enum.IsDefined(typeof(TransportTypes), value);
            if (!isValidEnum)
            {
                return BadRequest("Type of transport doesn't exist");
            }
            var trasnportId = parkingService.PutTransportInParking(value);
            if (trasnportId == Guid.Empty)
            {
                return BadRequest("Lots are full");
            }
            return CreatedAtAction("Get", new {id = trasnportId} , value);
        }

        // PUT api/values/5
        [HttpPatch("{id}")]
        public IActionResult Patch(Guid id, [FromBody] double value)
        {
            var targetTransport = parkingService.ParkedTransport.FirstOrDefault(i => i.TransportItemID == id);
            if (targetTransport == null)
            {
                return NotFound();
            }
            targetTransport.TopUpAccount(value);
            return NoContent();

        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            var targetTransport = parkingService.ParkedTransport.FirstOrDefault(i => i.TransportItemID == id);
            if (targetTransport == null)
            {
                return NotFound();
            }
            bool isRemoved = parkingService.RemoveTransportFromParking(targetTransport.TransportItemID);
            if (!isRemoved)
            {
                return BadRequest("Impossible to remove");
            }
            return NoContent();
        }
    }
}