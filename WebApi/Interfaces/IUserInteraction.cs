
using System;
using System.Collections.Generic;
using WebApi.BL.Models;
using WebApi.TypesEnum;

namespace WebApi.Interfaces{
public interface IUserInteraction
    {
        double BalanceOfParking { get;}
        double LastMinuteIncome { get; }
        int FreeLotsNumber { get; }
        int BusyLotsNumber { get; }
        IEnumerable<ITransportItem> ParkedTransport { get; }
        IEnumerable<Transaction> OutputLastMinuteTransactions();
        IEnumerable<string> OutputTransactionFromFile();
        Guid PutTransportInParking(TransportTypes transportType);
        bool RemoveTransportFromParking(Guid transportId);
        bool TopUpTransportAccount(Guid transportID, double topUpSum);

    }
}