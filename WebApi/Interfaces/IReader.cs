
using System.Collections.Generic;

namespace WebApi.Interfaces{
public interface IReader
    {
        IEnumerable<string> ReadLogs();
    }
}