
using System.Collections.Generic;
using WebApi.BL.Models;

namespace WebApi.Interfaces{
public interface ILogger
    {
        void WriteLogs(IEnumerable<Transaction> transactions);
    }
}