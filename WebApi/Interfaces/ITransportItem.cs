
using System;
using WebApi.BL.Models;

namespace WebApi.Interfaces{
public interface ITransportItem
    {
        Guid TransportItemID { get; }
        double Balance { get;  }
        Transaction PayForParking(IParking parking);
        void TopUpAccount(double acceptedSum);
    }
}