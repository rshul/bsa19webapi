
using System;
using System.Collections.Generic;
using WebApi.BL.Models;
using WebApi.TypesEnum;

namespace WebApi.Interfaces
{
    public interface IParking
    {
        double BalanceOfParking { get; }
        int NumberOfLots { get; }
        IEnumerable<ITransportItem> ParkedTransport { get; }
        IEnumerable<Transaction> LastTransactions { get; }
        int AvailableLotsNumber { get; }
        Guid PutTransportInParking(ITransportItem transportItem);
        bool RemoveTransportFromParking(Guid transportId);
        void GetPaymentsFromTransports();
        IReadOnlyDictionary<TransportTypes, double> Tariffs { get; }
        double ParkingFine { get; }
        ITransportItem GetTransportById(Guid transportId);
    }
}