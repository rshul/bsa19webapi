
using System.Collections.Generic;
using System.IO;
using WebApi.TypesEnum;

namespace WebApi.Settings
{
 public static class ParkingConfiguration
    {
        public static double ParkingBalance { get; } = 0;
        public static int ParkingCapacity { get; } = 10;
        public static int PeriodOfCharging { get; } = 5;
        public static IReadOnlyDictionary<TransportTypes, double> PriceForParking { get; } = new Dictionary<TransportTypes, double>()
        {
            {TransportTypes.PessengerCar, 2.0 },
            {TransportTypes.Truck, 5.0 },
            {TransportTypes.Bus, 3.5 },
            {TransportTypes.Bike, 1.0 },

        };
        public static double FineCoef { get; } = 2.5;
        public static string LogPath { get; }

        static ParkingConfiguration()
        {
            string currentDir = Directory.GetCurrentDirectory();
            LogPath = Path.Combine(currentDir, "Transactions.txt");
        } 

    }
}