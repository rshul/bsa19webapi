


namespace WebApi.TypesEnum
{
    public enum TransportTypes
    {
        PessengerCar,
        Truck,
        Bus,
        Bike
    }

}