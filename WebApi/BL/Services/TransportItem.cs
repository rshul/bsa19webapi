
using System;
using WebApi.BL.Models;
using WebApi.Interfaces;
using WebApi.TypesEnum;

namespace WebApi.BL.Services{
 public class TransportItem : ITransportItem
    {
        public Guid TransportItemID { get; } = Guid.NewGuid();

        public double Balance { get; private set; } = 50;
        public readonly TransportTypes transportType;

        public TransportItem(TransportTypes transportType)
        {
            this.transportType = transportType;
        }

        public Transaction PayForParking(IParking parking)
        {

            if (parking.Tariffs.TryGetValue(transportType, out double sumToPay))
            {
                if (sumToPay > Balance)
                {
                    sumToPay *= parking.ParkingFine;
                }
                Balance -= sumToPay;
                return new Transaction(TransportItemID, sumToPay);
            }
            return null;
        }

        public void TopUpAccount(double acceptedSum)
        {
            Balance += acceptedSum;
        }



        public override string ToString()
        {
            return $"# {TransportItemID.ToString("B")}; balance: {Balance}; type: {transportType};";
        }

    }   
}