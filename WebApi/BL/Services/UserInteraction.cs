
using System;
using System.Collections.Generic;
using System.Linq;
using WebApi.BL.Models;
using WebApi.Interfaces;
using WebApi.TypesEnum;

namespace WebApi.BL.Services
{
    public class UserInteraction : IUserInteraction
    {
        private IParking Parking { get; set; }
        public double BalanceOfParking => Parking.BalanceOfParking;
        public double LastMinuteIncome => Parking.LastTransactions.Select(x => x.TransactionSum).Sum();
        public int FreeLotsNumber => Parking.AvailableLotsNumber;
        public int BusyLotsNumber => Parking.NumberOfLots - Parking.AvailableLotsNumber;
        public IEnumerable<ITransportItem> ParkedTransport => Parking.ParkedTransport;
        private readonly IReader reader;

        public UserInteraction(IParking parking, IReader reader)
        {
            Parking = parking;
            this.reader = reader;
        }

        public IEnumerable<Transaction> OutputLastMinuteTransactions()
        {
            return Parking.LastTransactions;
        }

        public IEnumerable<string> OutputTransactionFromFile()
        {
            IEnumerable<string> lines = reader.ReadLogs();
            return lines;
        }

        public Guid PutTransportInParking(TransportTypes transportType)
        {
            return Parking.PutTransportInParking(new TransportItem(transportType));
        }

        public bool RemoveTransportFromParking(Guid transportId)
        {
            return Parking.RemoveTransportFromParking(transportId);
        }

        public bool TopUpTransportAccount(Guid transportID, double topUpSum)
        {
            if (topUpSum < 0 || topUpSum > 1000000)
            {
                return false;
            }
            var TransportItem = Parking.GetTransportById(transportID);
            if (TransportItem == null)
            {
                return false;
            }
            TransportItem.TopUpAccount(topUpSum);
            return true;
        }
    }
}