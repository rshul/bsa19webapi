
using System;
using System.Collections.Generic;
using System.IO;
using WebApi.Interfaces;
using WebApi.Settings;

namespace WebApi.BL.Services{
    public class LogReader : IReader
    {
        private readonly string Path = ParkingConfiguration.LogPath;

        public IEnumerable<string> ReadLogs()
        {
            List<string> outPutResult = new List<string>();
            try
            {

                using (var reader = new StreamReader(Path))
                {
                    while (!reader.EndOfStream)
                    {
                        outPutResult.Add(reader.ReadLine());
                    }
                }
            }
            catch(FileNotFoundException exception)
            {
                return null;
            }catch(Exception exception)
            {
                return null;
            }

            return outPutResult;
        }
    }
}