
using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using WebApi.BL.Models;
using WebApi.Interfaces;
using WebApi.Settings;
using WebApi.TypesEnum;

namespace WebApi.BL.Services{
  public class Parking : IParking
    {
        public Parking() {
            timerToCharge = new Timer(ParkingConfiguration.PeriodOfCharging * 1000);
            timerToCharge.Elapsed += OnTimeToCharge;
            timerToCharge.Start();
            timerToWriteLogs = new Timer( 60 * 1000);
            timerToWriteLogs.Elapsed += OnTimeToWriteLogsToFile;
            timerToWriteLogs.Start();

        }
        
        public double BalanceOfParking { get; private set; }
        public  int NumberOfLots => ParkingConfiguration.ParkingCapacity;
        private List<ITransportItem> TransportItems { get; } = new List<ITransportItem>();
        public IEnumerable<ITransportItem> ParkedTransport => TransportItems;
        private List<Transaction> LastTransactionsRegister { get; } = new List<Transaction>();
        public IEnumerable<Transaction> LastTransactions => LastTransactionsRegister;
        public int AvailableLotsNumber => NumberOfLots - TransportItems.Count;
        public IReadOnlyDictionary<TransportTypes, double> Tariffs => ParkingConfiguration.PriceForParking;
        public double ParkingFine => ParkingConfiguration.FineCoef;
        private readonly Timer timerToCharge;
        private readonly Timer timerToWriteLogs;

        private ILogger LogWriter { get; } = Logger.Intstance;

        private void OnTimeToCharge(object source, ElapsedEventArgs e)
        {
            GetPaymentsFromTransports();
        }

        private void OnTimeToWriteLogsToFile(object source, ElapsedEventArgs e)
        {
            if (LastTransactionsRegister.Count == 0)
            {
                return;
            }
            LogWriter.WriteLogs(LastTransactions.ToArray());
            LastTransactionsRegister.Clear();
        }

        public void GetPaymentsFromTransports()
        {
            foreach (var transportItem in TransportItems)
            {
               Transaction transaction = transportItem.PayForParking(this);
                if (transaction == null)
                {
                    continue;
                }
                BalanceOfParking += transaction.TransactionSum;
                LastTransactionsRegister.Add(transaction);
            }
        }

        public Guid PutTransportInParking(ITransportItem transportItem)
        {
            if (AvailableLotsNumber == 0)
            {
                return Guid.Empty;
            }
            TransportItems.Add(transportItem);
            return transportItem.TransportItemID;
        }

        public bool RemoveTransportFromParking(Guid transportId)
        {
            if (TransportItems.Count == 0)
            {
                return false;
            }
            var targetTransport = GetTransportById(transportId);
            if (targetTransport == null)
            {
                return false;
            }
            TransportItems.Remove(targetTransport);
            return true;
        }

        public ITransportItem GetTransportById(Guid transportId){
            var selectedTransport = TransportItems.FirstOrDefault(i=> i.TransportItemID == transportId);
            return selectedTransport;
        }
    }   
}