
using System;

namespace WebApi.BL.Models
{

    public class Transaction
    {
        public Guid TransactionId { get; } = Guid.NewGuid();
        public DateTime TransactionDateTime { get; set; }
        public Guid TransportId { get; set; }
        public double TransactionSum { get; set; }

        public Transaction(Guid transportId, double transactionSum)
        {
            TransportId = transportId;
            TransactionSum = transactionSum;
            TransactionDateTime = DateTime.Now;
        }

        public override string ToString()
        {
            return $"transportId: {TransportId}; dateTime: {TransactionDateTime}; sum: {TransactionSum}";
        }

    }
}